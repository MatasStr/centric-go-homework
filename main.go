package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"time"
)

func main() {

	http.HandleFunc("/measure", MeasureLatency)

	fmt.Println("Starting the server on port 8085...")
	log.Fatal(http.ListenAndServe(":8085", nil))
}

// CustomResponse is a struct with needed fields for the json response
type CustomResponse struct {
	Host     string  `json:"host"`
	Protocol string  `json:"protocol"`
	Results  Results `json:"results"`
}

// Results is a struct that holds measurements slice and the their average
type Results struct {
	Measurements   []string `json:"measurements"`
	AverageLatency string   `json:"averageLatency"`
}

// MeasureLatency is a handler for /measure url
func MeasureLatency(w http.ResponseWriter, r *http.Request) {

	// Getting query parameters
	host := r.URL.Query().Get("host")
	protocol := r.URL.Query().Get("protocol")
	samples, err := strconv.Atoi(r.URL.Query().Get("samples"))
	if err != nil {
		fmt.Println("Error converting samples parameter to int: ", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Could not convert samples parameter to int."))
		return
	}

	// Checking if protocol is valid
	if protocol != "http" && protocol != "https" {
		fmt.Println("Error: Wrong protocol passed via query parameters")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Unsupported protocol passed via query parameters. Allowed values are: http, https."))
		return
	}

	// Initial ping to find out if the host is reachable
	_, err = Ping(host, protocol)
	if err != nil {
		if strings.Contains(err.Error(), "timeout") {
			w.Write([]byte("Could not reach host. Request timeout."))
			fmt.Println("Error pinging ", host, ": ", err)
			return
		} else if strings.Contains(err.Error(), "no such host") {
			w.Write([]byte("No such host."))
			fmt.Println("Error pinging ", host, ": ", err)
			return
		} else {
			w.Write([]byte("Could not reach host."))
			fmt.Println("Error pinging ", host, ": ", err)
			return
		}
	}

	// Setting header to let the requester know that we will return JSON
	w.Header().Set("Content-Type", "application/json")

	// Initializing a new structure of our custom response object
	resp := CustomResponse{
		Host:     host,
		Protocol: protocol,
		Results:  RunSamples(host, protocol, samples),
	}

	// Converting out custom object data into a JSON object
	jsonObject, err := json.Marshal(resp)
	if err != nil {
		fmt.Println("json.Marshal() error:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Writing output
	w.Write(jsonObject)
}

// RunSamples is a function that accepts <host>, <protocol> and <samples> parameters and returns <Results> struct
func RunSamples(host string, protocol string, samples int) Results {

	var result Results
	var totalLatency time.Duration
	var chans = []chan time.Duration{}

	// Initializing goroutines to test latency
	for i := 0; i < samples; i++ {
		ch := make(chan time.Duration)
		chans = append(chans, ch)
		go PingWithChannel(host, protocol, ch)
	}

	cases := make([]reflect.SelectCase, len(chans))
	for i, ch := range chans {
		cases[i] = reflect.SelectCase{Dir: reflect.SelectRecv, Chan: reflect.ValueOf(ch)}
	}

	remaining := len(cases)
	for remaining > 0 {
		chosen, value, ok := reflect.Select(cases)
		if !ok {
			// The chosen channel has been closed, so zero out the channel to disable the case
			cases[chosen].Chan = reflect.ValueOf(nil)
			remaining--
			continue
		}

		totalLatency += value.Interface().(time.Duration)
		result.Measurements = append(result.Measurements, value.Interface().(time.Duration).Round(time.Millisecond).String())
	}

	if samples != 0 {
		result.AverageLatency = (totalLatency / (time.Millisecond * time.Duration(samples)) * time.Millisecond).Round(time.Millisecond).String()
	}

	return result
}

// Initializing a client for connections
var client = http.Client{
	Transport: &http.Transport{
		Dial: func(network, addr string) (net.Conn, error) {
			return net.DialTimeout(network, addr, 10*time.Second)
		},
	},
}

// PingWithChannel is a wrapper for Ping() function to return goroutine results via channel
func PingWithChannel(host string, protocol string, ch chan<- time.Duration) {

	latency, err := Ping(host, protocol)
	if err != nil {
		fmt.Println("Error with Ping(): ", err)
	}

	ch <- latency
	close(ch)
}

// Ping is a function that makes a request to <host> url with <protocol> protocol and returns latency
func Ping(host string, protocol string) (time.Duration, error) {

	// Building url string and making a new request
	url := protocol + "://" + host
	req, err := http.NewRequest("HEAD", url, nil)
	if err != nil {
		return 0, err
	}

	// Initial ping to get more accurate data
	resp, err := client.Do(req)
	if err != nil {
		return 0, err
	}

	// Starting timer
	start := time.Now()

	resp, err = client.Do(req)
	if err != nil {
		return 0, err
	}

	// Stopping timer
	elapsed := time.Since(start)

	// Closing response body
	resp.Body.Close()

	return elapsed, nil
}
