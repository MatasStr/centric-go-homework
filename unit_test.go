package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestRunSamples(t *testing.T) {

	host := "facebook.com"
	protocol := "https"
	samples := 5

	results := RunSamples(host, protocol, samples)
	if len(results.Measurements) != samples {
		t.Errorf("Amount of samples is incorrect, got: %d, expected: %d.", len(results.Measurements), samples)
	}

	var totalLatency time.Duration
	for _, measurement := range results.Measurements {
		duration, err := time.ParseDuration(measurement)
		if err != nil {
			t.Errorf("Error parsing measurement string to time.Duration: %v", err)
		}
		totalLatency += duration
	}

	averageLatency := (totalLatency / (time.Millisecond * time.Duration(samples)) * time.Millisecond).Round(time.Millisecond).String()
	if averageLatency < results.AverageLatency {
		t.Errorf("Calculated average latency is incorrect, got: %s, expected: %s.", averageLatency, results.AverageLatency)
	}
}

func TestPing(t *testing.T) {

	host := "facebook.com"
	protocol := "https"

	latency, err := Ping(host, protocol)
	if err != nil {
		t.Errorf("Error calling Ping() func: %v", err)
	}

	if latency == 0 {
		t.Errorf("Ping() returned 0 latency")
	}
}

func TestMeasureHandlerSuccess(t *testing.T) {

	req, err := http.NewRequest("GET", "/measure?host=facebook.com&protocol=https&samples=0", nil)
	if err != nil {
		t.Fatal("Error making a new request: ", err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(MeasureLatency)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	expected := `{"host":"facebook.com","protocol":"https","results":{"measurements":null,"averageLatency":""}}`

	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}
}

func TestMeasureHandlerNoSuchHost(t *testing.T) {

	req, err := http.NewRequest("GET", "/measure?host=nepasiekiamashostas123tikrai.com&protocol=https&samples=0", nil)
	if err != nil {
		t.Fatal("Error making a new request: ", err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(MeasureLatency)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	expected := `No such host.`

	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}
}

func TestMeasureHandlerTimeout(t *testing.T) {

	req, err := http.NewRequest("GET", "/measure?host=armandasbarkauskas.com&protocol=https&samples=0", nil)
	if err != nil {
		t.Fatal("Error making a new request: ", err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(MeasureLatency)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	expected := `Could not reach host. Request timeout.`

	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}
}

func TestMeasureHandlerBadSamplesInput(t *testing.T) {

	req, err := http.NewRequest("GET", "/measure?host=nepasiekiamashostas123tikrai.com&protocol=https&samples=i", nil)
	if err != nil {
		t.Fatal("Error making a new request: ", err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(MeasureLatency)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusInternalServerError {
		t.Errorf("Handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}
}

func TestMeasureHandlerBadProtocolInput(t *testing.T) {

	req, err := http.NewRequest("GET", "/measure?host=nepasiekiamashostas123tikrai.com&protocol=randomprotocol&samples=1", nil)
	if err != nil {
		t.Fatal("Error making a new request: ", err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(MeasureLatency)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusInternalServerError {
		t.Errorf("Handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}
}
